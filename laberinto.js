// Array con todas las posiciones válidas en las que puede estar el minion
let coordenadas = [
    [654, 319.5],
    [614, 319.5],
    [574, 319.5],
    [574, 279.5],
    [574, 239.5],
    [574, 199.5],
    [574, 159.5],
    [574, 119.5],
    [574, 79.5],
    [574, 39.5],
    [534, 39.5],
    [534, 79.5],
    [494, 79.5],
    [494, 39.5],
    [454, 39.5],
    [414, 39.5],
    [494, 79.5],
    [494, 119.5],
    [494, 159.5],
    [454, 159.5],
    [414, 159.5],
    [414, 199.5],
    [414, 239.5],
    [454, 239.5],
    [494, 239.5],
    [494, 279.5],
    [494, 319.5],
    [454, 319.5],
    [414, 319.5],
    [374, 319.5],
    [334, 319.5],
    [334, 359.5],
    [334, 399.5],
    [334, 439.5],
    [334, 479.5],
    [374, 399.5],
    [414, 399.5],
    [454, 399.5],
    [494, 399.5],
    [534, 399.5],
    [574, 399.5],
    [574, 439.5],
    [574, 479.5],
    [574, 519.5],
    [574, 559.5],
    [574, 599.5],
    [534, 599.5],
    [494, 599.5],
    [494, 559.5],
    [494, 519.5],
    [494, 479.5],
    [454, 479.5],
    [414, 479.5],
    [414, 519.5],
    [414, 559.5],
    [414, 599.5],
    [374, 599.5],
    [334, 599.5],
    [294, 599.5],
    [254, 599.5],
    [214, 599.5],
    [174, 599.5],
    [134, 599.5],
    [94, 599.5],
    [54, 599.5],
    [54, 559.5],
    [54, 519.5],
    [54, 479.5],
    [94, 479.5],
    [134, 479.5],
    [54, 479.5],
    [94, 479.5],
    [134, 479.5],
    [134, 439.5],
    [134, 339.5],
    [214, 559.5],
    [214, 519.5],
    [214, 479.5],
    [214, 559.5],
    [214, 519.5],
    [214, 479.5],
    [214, 439.5],
    [214, 399.5],
    [214, 359.5],
    [214, 319.5],
    [214, 279.5],
    [214, 239.5],
    [254, 239.5],
    [294, 239.5],
    [334, 239.5],
    [334, 199.5],
    [334, 159.5],
    [334, 119.5],
    [334, 79.5],
    [334, 39.5],
    [294, 39.5],
    [254, 39.5],
    [214, 39.5],
    [174, 39.5],
    [134, 39.5],
    [94, 39.5],
    [54, 39.5],
    [54, 79.5],
    [54, 119.5],
    [54, 159.5],
    [54, 199.5],
    [54, 239.5],
    [214, 79.5],
    [214, 119.5],
    [214, 159.5],
    [174, 159.5],
    [134, 159.5],
    [134, 199.5],
    [134, 239.5],
    [134, 279.5],
    [134, 319.5],
    [94, 319.5],
    [54, 319.5],
    [54, 359.5],
    [54, 399.5],
    [14, 399.5],
];

$(function () {
    $(document).keyup(function (tecla) {
        // derecha 39
        // izquierda 37
        // arriba 38
        // abajo 40

        // Posicion de la imagen del minion
        posicionX = $('.minion').position().left;
        posicionY = $('.minion').position().top;

        // Si se pulsa la tecla derecha 
        if (tecla.keyCode == 39) {
            // comprobamos si la posicion siguiente es valida
            if (posicionViable(posicionX + 40, posicionY)) {
                // si es válida, nos movemos a la derecha 40px
                $(".minion").animate({ left: '+=40px' });
            }
        }

        // Si se pulsa la tecla izquierda 
        if (tecla.keyCode == 37) {
            if (posicionViable(posicionX - 40, posicionY)) {
                $(".minion").animate({ left: '-=40px' });
            }
        }


        // Si se pulsa la tecla arriba 
        if (tecla.keyCode == 38) {
            if (posicionViable(posicionX, posicionY - 40)) {
                $(".minion").animate({ top: '-=40px' });
            }
        }

        // Si se pulsa la tecla abajo 
        if (tecla.keyCode == 40) {
            if (posicionViable(posicionX, posicionY + 40)) {
                $(".minion").animate({ top: '+=40px' });
            }
        }


        // Si llega al final
        if (tecla.keyCode == 37 && posicionX == 14 && posicionY == 399.5) {
            // mostrar mensaje
            alert("Enhorabuena has llegado");
            // cargar página de nuevo
            location.reload();
        }

    });
});

/**
 * Función que comprueba si la posición que le pasas por argumento está en el array de coordena.toDateString()
 * Si está => devuelve true
 */
function posicionViable(posicionH, posicionV) {
    // recorremos el array de coordenadas 
    for (let i = 0; i < coordenadas.length; i++) {
        // Si la posicionH y posicionV están en el array devolvemos true
        if (posicionH == coordenadas[i][0] && posicionV == coordenadas[i][1]) {
            return true;
        }
    }
}
